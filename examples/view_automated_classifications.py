import tensorflow as tf

from fieldmap_classifier.helpers.view_classification import view_automated_classification
from train_classifier import classifier_class, client

if __name__ == '__main__':
    tf.config.set_visible_devices([], 'GPU')
    # weight_file keyword is not given so this function will use the latest weight file
    # for the given classifier
    view_automated_classification(classifier_class, client)
