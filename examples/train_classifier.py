import keyring
from fieldmaptools import FieldmapsClient

# noinspection PyUnresolvedReferences
from fieldmap_classifier import \
    HeadCoilB1SubjectClassifier, \
    HeadCoilB0SubjectClassifier, \
    HeadCoilPtxSubjectClassifier, \
    FieldProbeCoilB1SubjectClassifier, \
    FieldProbeCoilB0SubjectClassifier, \
    FieldProbeCoilPtxSubjectClassifier, \
    BroadB1SubjectClassifier, \
    BroadB0SubjectClassifier, \
    BroadPtxSubjectClassifier, \
    BroadSubjectClassifier

classifier_class = BroadSubjectClassifier
data_dir = '/storage/akuurstr/fieldmaps'
before_study = 561
client = FieldmapsClient('https://fieldmap.cfmm.uwo.ca', keyring.get_password('fieldmaps auth_code', 'akuurstr'))

if __name__ == '__main__':
    classifier_weight_file = classifier_class().train(
        client,
        data_dir,
        epochs=4000,
        before_study=before_study,
        validation_split=0.3,
        # sync_data_dir=True,
        show_plots=True,
    )
