import numpy as np
import tensorflow as tf

from fieldmap_classifier.helpers.view_classification import view_uncertain_classifications
from fieldmap_classifier.subject_classifiers import get_scan_paths
from train_classifier import classifier_class, data_dir, before_study, client

tf.config.set_visible_devices([], 'GPU')
human_scan_paths = []
nonhuman_scan_paths = []

coil_info = client.list_all_as_structured_array('coil')
indexes = np.logical_or.reduce([coil_info['coilname'] == c for c in classifier_class.coils])
coil_ids = coil_info[indexes]['id'].tolist()
for imgtype in classifier_class.filetypes:
    human, nonhuman = get_scan_paths(imgtype, coil_ids, client, data_dir, before_study=before_study)
    human_scan_paths.extend(human)
    nonhuman_scan_paths.extend(nonhuman)
scan_paths = human_scan_paths + nonhuman_scan_paths
scan_labels = ['human'] * len(human_scan_paths) + ['nonhuman'] * len(nonhuman_scan_paths)
# weight_file keyword is not given so this function will use the latest weight file
# for the given classifier
view_uncertain_classifications(classifier_class, scan_paths, scan_labels)
