from fieldmaptools import FieldmapsClient
import keyring
from fieldmap_classifier.helpers.manual_classification import classify_study

auth_code = keyring.get_password('fieldmaps auth_code', 'akuurstr')
client = FieldmapsClient('https://fieldmap.cfmm.uwo.ca', auth_code)
classify_study(client, 'human', after_study_id=560)