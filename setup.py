import os
import setuptools

setuptools.setup(
    name="fieldmap_classifier",
    version=os.getenv('CI_COMMIT_TAG') or '0.1',
    author="Alan Kuurstra",
    author_email="akuurstr@uwo.ca",
    description="Tools for classifying CFMM fieldmaps.",
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Kuurstra/fieldmap_classifier",
    packages=setuptools.find_packages(),
    package_data={'fieldmap_classifier': ['saved_weights/*.h5']},
    include_package_data=True,
    # note that fieldmaptools requires --extra-index-url https://gitlab.com/api/v4/projects/24717924/packages/pypi/simple
    install_requires=[
        'numpy>=1.19.1',
        'tensorflow>=2.5.0',
        'fieldmaptools>=0.2.6',
    ],
    extras_require={
        "visual": [
            'matplotlib>=3.4.2',
            'vidi3d==1.2.1',
        ],
        "keyring": ['keyring>=23.0.1']
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.8',
)
