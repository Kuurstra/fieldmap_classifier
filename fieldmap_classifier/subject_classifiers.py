from datetime import datetime, timezone, timedelta
from pathlib import Path

import numpy as np
from fieldmaptools import get_reader, B0Reader, B1Reader
from fieldmaptools.server_models import B0, B1, PTx

from .base_fieldmap_classifier import FieldmapClassifier, err
from .helpers.core import get_raw_sos, get_stats
from .models import get_model


def get_ids(imgtype, client, after_study=0, before_study=np.inf, coil_ids=None):
    coil_ids = [2] if coil_ids is None else coil_ids
    # get a list of studies using human head coil
    studies = client.query_return_structured_array('study', coil_ids=coil_ids)
    human_ids = []
    nonhuman_ids = []
    for study in studies:
        if study['id'] <= after_study:
            continue
        elif study['id'] == before_study:
            break
        imgtype_relationship_name = 'ptx' if imgtype == 'ptxpulsedesign' else imgtype
        if 'human' in study['tags']:
            human_ids.extend(study[f'{imgtype_relationship_name}_ids'])
        elif 'nonhuman' in study['tags']:
            nonhuman_ids.extend(study[f'{imgtype_relationship_name}_ids'])

    human_ids.sort()
    nonhuman_ids.sort()

    return human_ids, nonhuman_ids


def get_scan_paths(imgtype, coil_ids, client, data_dir, sync_data_dir=True, before_study=np.inf):
    data_dir = Path(data_dir)

    # sync local image directories
    if sync_data_dir:
        if before_study < np.inf:
            # noinspection PyProtectedMember
            rv = client._record_list_to_structured_array([client.info('study', before_study - 1)])
            study_time = rv['generated'][0].astype(datetime).astimezone(timezone.utc) + timedelta(hours=1)
            client.query_save(imgtype, data_dir, coil_ids=coil_ids, generated=(None, study_time))
        else:
            client.query_save(imgtype, data_dir, coil_ids=coil_ids)

    human_ids, nonhuman_ids = get_ids(imgtype, coil_ids=coil_ids, client=client, before_study=before_study)

    human_paths = [data_dir / f'{imgtype}_{x}.h5' for x in human_ids]
    nonhuman_paths = [data_dir / f'{imgtype}_{x}.h5' for x in nonhuman_ids]

    return human_paths, nonhuman_paths


class _BaseSubjectClassifier(FieldmapClassifier):
    classtype = 'subject'
    model_input_shape = property(err, err, err)
    classnames = ['human', 'nonhuman']

    def __init__(self, *args, **kwargs):
        super().__init__(get_model(*self.model_input_shape), *args, **kwargs)

    def get_data(self,
                 client,
                 data_dir,
                 sync_data_dir=False,
                 validation_split=0.3,
                 before_study=np.inf,
                 ):
        human_train = []
        human_val = []
        nonhuman_train = []
        nonhuman_val = []
        for coilname in self.coils:
            coils = client.list_all_as_structured_array('coil')
            coil_id = coils[coils['coilname'] == coilname][0]['id']
            for imgtype in self.filetypes:
                human_scans = []
                nonhuman_scans = []
                human_scan_paths, nonhuman_scan_paths = get_scan_paths(imgtype,
                                                                       [coil_id],
                                                                       client,
                                                                       data_dir,
                                                                       before_study=before_study,
                                                                       sync_data_dir=sync_data_dir, )

                for scan_paths, scans in zip([human_scan_paths, nonhuman_scan_paths], [human_scans, nonhuman_scans]):
                    for path in scan_paths:
                        try:
                            r = get_reader(path)
                            self.validate_metadata(r)
                            if isinstance(self, BroadSubjectClassifier) and isinstance(r, B1Reader):
                                # B1 dominates multi-image training due to imbalance in number of examples
                                # remove older b1 images (exception thrown because older b1 files don't contain raw data)
                                r.rawData
                            scans.append(self.get_image(r)[0])
                        except (AttributeError, ValueError) as e:
                            print(f'{path}: {str(e)}')

                # Split data into training and validation.
                n_human = len(human_scans)
                n_human_train = int(n_human * (1.0 - validation_split))
                human_train.extend(human_scans[:n_human_train])
                human_val.extend(human_scans[n_human_train:])

                n_nonhuman = len(nonhuman_scans)
                n_nonhuman_train = int(n_nonhuman * (1.0 - validation_split))
                nonhuman_train.extend(nonhuman_scans[:n_nonhuman_train])
                nonhuman_val.extend(nonhuman_scans[n_nonhuman_train:])

        print(
            "Number of samples in train and validation are %d and %d."
            % (len(human_train) + len(nonhuman_train), len(human_val) + len(nonhuman_val))
        )

        input_train = np.stack(human_train + nonhuman_train, axis=0)
        label_train = np.concatenate((np.ones(len(human_train), dtype=human_train[0].dtype),
                                      np.zeros(len(nonhuman_train), dtype=nonhuman_train[0].dtype)), axis=0)
        input_val = None
        label_val = None
        if validation_split:
            input_val = np.stack(human_val + nonhuman_val, axis=0)
            label_val = np.concatenate((np.ones(len(human_val), dtype=human_val[0].dtype),
                                        np.zeros(len(nonhuman_val), dtype=nonhuman_val[0].dtype)), axis=0)
        return input_train, label_train, input_val, label_val


class BroadSubjectClassifier(_BaseSubjectClassifier):
    filetypes = [B0.__api_name__, B1.__api_name__, PTx.__api_name__]
    coils = ['CFMM_HeadCoil', 'CFMM_FieldProbeCoil']
    matrix_sizes = [((0, np.inf), (0, np.inf), (0, np.inf), (0, np.inf)), ((0, np.inf), (0, np.inf), (0, np.inf))]
    fovs = [((0, np.inf), (0, np.inf), (0, np.inf))]
    model_input_shape = (64, 64, 25)

    def validate_metadata(self, reader):
        return all([
            self.validate_filetypes(reader.filetypes),
            self.validate_coil(reader.transmittingCoil),
            self.validate_matrix_size(reader.fieldmap.shape),
            self.validate_fov(reader.fov)
        ])

    @staticmethod
    def get_image(reader):
        cntr = np.array((0, 0, 0))
        fov = np.array((256, 256, 250))
        mtrx_sz = np.array(BroadSubjectClassifier.model_input_shape)
        img = reader.interpolate_zoom(fov=fov, center=cntr, matrix_size=mtrx_sz, bounds_error=False, fill_value=0)
        if isinstance(reader, B1Reader):
            img = np.abs(img.sum(axis=-1)).real

        if isinstance(reader, B0Reader):
            mag = reader.interpolate_zoom_rawdata_mag(fov=fov, center=cntr, matrix_size=mtrx_sz, bounds_error=False,
                                                      fill_value=0).sum(axis=-1)
            m, std = get_stats(mag)
            mask = np.abs(mag - m) < 1.5 * std
        else:
            mask = np.abs(img) > 0

        # standardize
        valid_stats = img[mask]
        m = valid_stats.mean()
        std = valid_stats.std()
        img = (img - m) / std

        return np.expand_dims(img, axis=0)


class _BaseB1SubjectClassifier(_BaseSubjectClassifier):
    filetypes = [B1.__api_name__]
    model_input_shape = (64, 64, 25)

    @staticmethod
    def get_image(reader):
        return np.expand_dims(get_raw_sos(reader), axis=0)

    def validate_metadata(self, reader):
        return all([
            self.validate_filetypes(reader.filetypes),
            self.validate_coil(reader.transmittingCoil),
            self.validate_matrix_size(reader.rawData.shape),
            self.validate_fov(reader.fov)
        ])


class BroadB1SubjectClassifier(_BaseB1SubjectClassifier):
    coils = ['CFMM_HeadCoil', 'CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25), (9, 9))]
    fovs = [((256, 256), (256, 256), (250, 250))]


class HeadCoilB1SubjectClassifier(_BaseB1SubjectClassifier):
    coils = ['CFMM_HeadCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25), (9, 9))]
    fovs = [((256, 256), (256, 256), (250, 250))]


class FieldProbeCoilB1SubjectClassifier(_BaseB1SubjectClassifier):
    coils = ['CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25), (9, 9))]
    fovs = [((256, 256), (256, 256), (250, 250))]


class _BaseB0SubjectClassifier(_BaseSubjectClassifier):
    filetypes = [B0.__api_name__]
    model_input_shape = (64, 64, 24)

    @staticmethod
    def get_image(reader):
        return np.expand_dims(get_raw_sos(reader), axis=0)

    def validate_metadata(self, reader):
        return all([
            self.validate_filetypes(reader.filetypes),
            self.validate_coil(reader.transmittingCoil),
            self.validate_matrix_size(reader.rawData.shape),
            self.validate_fov(reader.fov)
        ])


class BroadB0SubjectClassifier(_BaseB0SubjectClassifier):
    coils = ['CFMM_HeadCoil', 'CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (24, 24), (3, 3))]
    fovs = [((256, 256), (256, 256), (288, 288))]


class HeadCoilB0SubjectClassifier(_BaseB0SubjectClassifier):
    coils = ['CFMM_HeadCoil']
    matrix_sizes = [((64, 64), (64, 64), (24, 24), (3, 3))]
    fovs = [((256, 256), (256, 256), (288, 288))]


class FieldProbeCoilB0SubjectClassifier(_BaseB0SubjectClassifier):
    coils = ['CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (24, 24), (3, 3))]
    fovs = [((256, 256), (256, 256), (288, 288))]


class _BasePtxSubjectClassifier(_BaseSubjectClassifier):
    filetypes = [PTx.__api_name__]
    model_input_shape = (64, 64, 25)

    @staticmethod
    def get_image(reader):
        return np.expand_dims(reader.fieldmap, axis=0)

    def validate_metadata(self, reader):
        return all([
            self.validate_filetypes(reader.filetypes),
            self.validate_coil(reader.transmittingCoil),
            self.validate_matrix_size(reader.fieldmap.shape),
            self.validate_fov(reader.fov)
        ])


class BroadPtxSubjectClassifier(_BasePtxSubjectClassifier):
    coils = ['CFMM_HeadCoil', 'CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25))]
    fovs = [((256, 256), (256, 256), (250, 250))]


class HeadCoilPtxSubjectClassifier(_BasePtxSubjectClassifier):
    coils = ['CFMM_HeadCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25))]
    fovs = [((256, 256), (256, 256), (250, 250))]


class FieldProbeCoilPtxSubjectClassifier(_BasePtxSubjectClassifier):
    coils = ['CFMM_FieldProbeCoil']
    matrix_sizes = [((64, 64), (64, 64), (25, 25))]
    fovs = [((256, 256), (256, 256), (250, 250))]
