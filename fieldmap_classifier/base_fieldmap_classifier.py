import os
from datetime import datetime
from pathlib import Path

import numpy as np
import tensorflow as tf
from tensorflow import keras

from .callbacks import ModelCheckpoint
from .helpers.core import view_training_graphs

err = lambda *args, **kwargs: NotImplementedError


class FieldmapClassifier:
    classtype = None
    filetypes = property(err, err, err)
    coils = property(err, err, err)
    matrix_sizes = property(err, err, err)
    fovs = property(err, err, err)
    classnames = None

    def __init__(self, model, weight_file=None, min_confidence=0.9):
        self.model = model
        if weight_file is not None:
            self.load(weight_file)
        self.min_confidence = min_confidence

    def load(self, weight_file):
        self.model.load_weights(weight_file)

    @classmethod
    def validate_filetypes(cls, test_filetypes):
        valid = False
        for filetype in test_filetypes:
            if filetype in cls.filetypes:
                valid = True
        if not valid:
            raise ValueError(f'filetypes are {test_filetypes}, but at least one must be in {cls.filetypes}')
        return True

    @classmethod
    def validate_coil(cls, coil):
        if coil not in cls.coils:
            raise ValueError(f'coil is {coil}, but must be in {cls.coils}')
        return True

    @classmethod
    def validate_matrix_size(cls, matrix_size):
        valid_matrix_sizes = [x for x in cls.matrix_sizes if len(x) == len(matrix_size)]
        for valid_matrix_size in valid_matrix_sizes:
            is_valid = True
            for dim_indx in range(len(matrix_size)):
                is_valid = is_valid and (
                        matrix_size[dim_indx] >= valid_matrix_size[dim_indx][0] and matrix_size[dim_indx] <=
                        valid_matrix_size[dim_indx][1])
            if is_valid:
                return True
        raise ValueError(
            f'matrix size is {matrix_size}, but must be in one of the following ranges {cls.matrix_sizes}')

    @classmethod
    def validate_fov(cls, fov):
        valid_fovs = [x for x in cls.fovs if len(x) == len(fov)]
        for valid_fov in valid_fovs:
            if fov[0] >= valid_fov[0][0] and fov[0] <= valid_fov[0][1] \
                    and fov[1] >= valid_fov[1][0] and fov[1] <= valid_fov[1][1] \
                    and fov[2] >= valid_fov[2][0] and fov[2] <= valid_fov[2][1]:
                return True
        raise ValueError(
            f'fov is {fov}, but must be in one of the following ranges {cls.fovs}')

    @classmethod
    def validate_metadata(cls, reader):
        raise NotImplementedError

    @classmethod
    def get_image(cls, reader):
        raise NotImplementedError

    @classmethod
    def get_data(cls,
                 client,
                 data_dir,
                 sync_data_dir=False,
                 validation_split=0.3,
                 ):
        raise NotImplementedError

    def compile(self, lr_schedule):
        # see tf.keras.metrics for more metrics that can be tracked
        self.model.compile(
            loss='binary_crossentropy',
            optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
            metrics=['accuracy'],
        )

    def train(self,
              client,
              data_dir,
              sync_data_dir=False,
              before_study=np.inf,
              validation_split=0.3,  # train/validation split
              epochs=10000,
              batch_size=32,  # batch_size depends on gpu memory and size of model
              lr_schedule=keras.optimizers.schedules.ExponentialDecay(
                  initial_learning_rate=0.0001, decay_steps=100000, decay_rate=0.96, staircase=True),
              saved_weights_dir=Path(__file__).parent / 'saved_weights',
              show_plots=False,
              ):
        saved_weights = Path(saved_weights_dir, self.get_weight_filename())

        input_train, label_train, input_val, label_val = self.get_data(client, data_dir, sync_data_dir=sync_data_dir,
                                                                       before_study=before_study,
                                                                       validation_split=validation_split)

        def preprocessing(volume, label):
            volume = tf.expand_dims(volume, axis=3)
            return volume, label

        # Define data loaders.
        train_loader = tf.data.Dataset.from_tensor_slices((input_train, label_train))

        # Could potentially augment data on the fly during training.
        train_dataset = (
            train_loader.shuffle(len(input_train))
                .map(preprocessing)
                .batch(batch_size)
                .prefetch(2)
        )

        if validation_split:
            validation_loader = tf.data.Dataset.from_tensor_slices((input_val, label_val))
            validation_dataset = (
                validation_loader.shuffle(len(input_val))
                    .map(preprocessing)
                    .batch(batch_size)
                    .prefetch(2)
            )
        else:
            validation_dataset = None

        self.compile(lr_schedule)

        # Train the model, doing validation at the end of each epoch
        self.model.fit(
            train_dataset,
            validation_data=validation_dataset,
            epochs=epochs,
            shuffle=True,
            verbose=2,
            callbacks=[ModelCheckpoint(saved_weights, monitor_validation=validation_split)],
        )

        if show_plots:
            view_training_graphs(self.model.history, validation_split)

        return saved_weights

    def classify(self, reader):
        if not self.validate_metadata(reader):
            return None
        img = self.get_image(reader)
        return self.classify_img(img)

    def classify_img(self, img):
        probability_vector = self.model.predict(img)[0]
        # put binary classification into a 2-vector
        if len(probability_vector) == 1:
            probability_vector = [probability_vector[0], 1 - probability_vector[0]]

        n_classes = len(probability_vector)
        classnames = self.classnames
        if classnames is None:
            classnames = [f'class_{n}' for n in n_classes]
        assert len(classnames) == len(probability_vector)
        class_probabilities = {classname: probability for classname, probability in zip(classnames, probability_vector)}
        argmax = np.argmax(probability_vector)
        if probability_vector[argmax] >= self.min_confidence:
            classname = classnames[argmax]
        else:
            classname = 'unclassified'
        info = {'class_probabilities': class_probabilities, 'min_confidence': self.min_confidence}
        return classname, info

    @classmethod
    def _get_filename_info(cls):
        imgtype = cls.filetypes[0] if len(cls.filetypes) == 1 else 'broad'
        imgtype = imgtype.replace('_', '').lower()

        coiltype = cls.coils[0] if len(cls.coils) == 1 else 'broad'
        coiltype = coiltype.replace('_', '').lower()

        classtype = cls.classtype.replace('_', '').lower()
        return imgtype, coiltype, classtype

    @classmethod
    def get_weight_filename(cls):
        imgtype, coiltype, classtype = cls._get_filename_info()
        return f"{coiltype}_{imgtype}_{classtype}_classifier_{datetime.now().isoformat(timespec='seconds').replace(':', '-')}.h5"

    @classmethod
    def get_latest_weight_file(cls, saved_weights_dir=None):
        imgtype, coiltype, classtype = cls._get_filename_info()

        if saved_weights_dir is None:
            saved_weights_dir = Path(__file__).parent / 'saved_weights'
        else:
            saved_weights_dir = Path(saved_weights_dir)
        latest_datetime = datetime.min
        latest_weights = None
        for weight_file in os.listdir(saved_weights_dir):
            tmp = weight_file.split('_')
            if len(tmp) < 3:
                continue
            current_coiltype = tmp[0]
            current_imgtype = tmp[1]
            current_classtype = tmp[2]
            if (coiltype and current_coiltype != coiltype) or \
                    (imgtype and current_imgtype != imgtype) or \
                    (classtype and current_classtype != classtype):
                continue
            try:
                current_datetime = datetime.strptime(tmp[-1].split('.')[0], '%Y-%m-%dT%H-%M-%S')
            except Exception as e:
                print(e)
                continue
            if current_datetime > latest_datetime:
                latest_datetime = current_datetime
                latest_weights = weight_file
        if latest_weights is None:
            raise FileNotFoundError(f"No {imgtype} model weight files found in {saved_weights_dir.absolute()}.")
        return saved_weights_dir / latest_weights

    def evaluate(self, data=None, lr_schedule=keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=0.0001,
                                                                                          decay_steps=100000,
                                                                                          decay_rate=0.96,
                                                                                          staircase=True), **kwargs):
        self.compile(lr_schedule)
        if data is not None:
            input_imgs, labels = data
        else:
            input_imgs, labels, _, _ = self.get_data(validation_split=0, **kwargs)

        print(self.model.evaluate(input_imgs, labels))
