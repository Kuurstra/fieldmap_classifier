import importlib
import inspect
import os
from pathlib import Path

import numpy as np


def get_raw_sos(reader):
    return np.sqrt((reader.rawData * reader.rawData.conj()).real.sum(axis=-1))


def get_b1_map_sum(b1_reader):
    return np.abs(b1_reader.fieldmap.sum(axis=-1)).real


def view_training_graphs(model_history, validation_data=True):
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots(1, 2, figsize=(20, 3))
    ax = ax.ravel()

    for i, metric in enumerate(["accuracy", "loss"]):
        ax[i].plot(model_history.history[metric])
        if validation_data:
            ax[i].plot(model_history.history["val_" + metric])
            ax[i].legend(["train", "val"])
        ax[i].set_title("Model {}".format(metric))
        ax[i].set_xlabel("epochs")
        ax[i].set_ylabel(metric)

    plt.show(block=True)


def get_classifiers(imgtype=None, classtype=None):
    # could add coil as filter
    classifier_dir = Path(__file__).parent / 'classifiers'
    for file in classifier_dir.glob("*.py"):
        module_name = os.path.splitext(file.name)[0]
        module = importlib.import_module('fieldmap_classifier.classifiers.' + module_name)
        for name, obj in inspect.getmembers(module):
            if inspect.isclass(obj) and hasattr(obj, 'classtype') and name[0] != '_':
                try:
                    if imgtype and imgtype not in obj.filetypes:
                        continue
                    if classtype and classtype != obj.classtype:
                        continue
                except NotImplementedError:
                    continue
                yield obj


def get_stats(img):
    valid_values = img[np.logical_and(np.isfinite(img), np.abs(img).astype(bool))]
    if valid_values.size == 0:
        raise Exception('no valid voxels')

    # use n_stdv*stdv for dynamic range
    stdv = valid_values[valid_values > valid_values.max() * 0.5].std()
    mean = valid_values[valid_values > valid_values.max() * 0.5].mean()
    n_stdv = 3

    # remove outliers
    stdv_prev = np.inf
    max_iter = 25
    count = 0
    while np.abs((stdv_prev - stdv) / stdv) > 0.1 and count < max_iter:
        if stdv == 0:
            raise Exception('no valid voxels left')
        valid_values = valid_values[np.abs(valid_values - mean) < n_stdv * stdv]
        mean = valid_values.mean()
        stdv_prev = stdv
        stdv = valid_values.std()
        count += 1
    return mean, stdv
