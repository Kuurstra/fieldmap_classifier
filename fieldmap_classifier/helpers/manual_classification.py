""""
get a list of all studies. view all associated images and mark the study as human if applicable.
use the custom viewer middle mouse button to do this.
note: it was found that some studies have both human and phantom scans.
"""
import io
from functools import partialmethod

import numpy as np
from PyQt5.QtWidgets import QMessageBox
from fieldmaptools import B1Reader, B0Reader, PtxPulseDesignReader, get_reader
from fieldmaptools.server_models import B1, B0, PTx
from vidi3d import compare3d as compare3d_orig

from fieldmap_classifier.helpers.core import get_b1_map_sum, get_raw_sos


def middle_mouse_fn(self, client, model_api_name, model_id, tag):
    msgBox = QMessageBox()
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setWindowTitle("Tagging")
    msgBox.setText(f"Tag {model_api_name}/{model_id} as {tag}?")
    msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
    returnValue = msgBox.exec()
    if returnValue == QMessageBox.Ok:
        rv = client.tag(model_api_name, model_id, tag)
        print(rv.content)


def compare3d(*args, client=None, model_api_name=None, model_id=None, tag=None, **kwargs):
    compare3d_orig(*args, **kwargs, mmb_callback=partialmethod(middle_mouse_fn, client, model_api_name, model_id, tag))


def classify_study(client, tag, after_study_id=0):
    study_records = client.list_all_as_structured_array('study', after=after_study_id)
    for study_record in study_records:
        study_id = study_record['id']
        print()
        print('study id:', study_id)
        print('b1 ids:', study_record['b1_ids'])
        print('b0 ids:', study_record['b0_ids'])
        print('ptx ids:', study_record['ptx_ids'])
        b1_readers = []
        b0_readers = []
        ptx_readers = []
        patient_name = None
        for b1_id in study_record['b1_ids']:
            rv = client.get('b1', b1_id)
            binary_stream = io.BytesIO(rv.content)
            f = B1Reader(binary_stream)
            b1_readers.append(f)
            patient_name = f.patientName

        for b0_id in study_record['b0_ids']:
            rv = client.get('b0', b0_id)
            binary_stream = io.BytesIO(rv.content)
            f = B0Reader(binary_stream)
            b0_readers.append(f)
            patient_name = f.patientName

        for ptx_id in study_record['ptx_ids']:
            rv = client.get('ptxpulsedesign', ptx_id)
            binary_stream = io.BytesIO(rv.content)
            f = PtxPulseDesignReader(binary_stream)
            ptx_readers.append(f)
            patient_name = f.patientName

        if not patient_name:
            patient_name = 'unknown'

        if b1_readers:
            compare3d(
                [get_b1_map_sum(f) for f in b1_readers],
                client=client,
                model_api_name='study',
                model_id=study_id,
                tag=tag,
                window_title=f"B1, study id: {study_id}, patient name: {patient_name}",
                subplot_titles=[f'b1fieldmap id: {x}' for x in study_record['b1_ids']],
                block=(not b0_readers and not ptx_readers)
            )
        if b0_readers:
            compare3d(
                [get_raw_sos(f) for f in b0_readers],
                client=client,
                model_api_name='study',
                model_id=study_id,
                tag=tag,
                window_title=f"B0, study id: {study_id}, patient name: {patient_name}",
                subplot_titles=[f'b0fieldmap id: {x}' for x in study_record['b0_ids']],
                block=(not ptx_readers),
            )

        if ptx_readers:
            compare3d(
                [f.fieldmap for f in ptx_readers],
                client=client,
                model_api_name='study',
                model_id=study_id,
                tag=tag,
                window_title=f"Ptx, study id: {study_id}, patient name: {patient_name}",
                subplot_titles=[f'ptxpulsedesign id: {x}' for x in study_record['ptx_ids']],
                block=True,
            )


def classify_model(client, model_api_name, tag, after_id=0):
    model_records = client.list_all_as_structured_array(model_api_name, after=after_id)
    for model_record in model_records:
        model_id = model_record['id']
        print()
        print(f"{model_api_name} id: {model_id}")
        rv = client.get(model_api_name, model_id)
        binary_stream = io.BytesIO(rv.content)
        f = get_reader(model_api_name)(binary_stream)
        img = None
        if model_api_name == B1.__api_name__:
            img = np.abs(f._reformat_img(f.fieldmap, 8)).mean(axis=-1)
        elif model_api_name == B0.__api_name__:
            img = np.abs(f.rawData).mean(axis=-1)
        elif model_api_name == PTx.__api_name__:
            img = f.fieldmap

        compare3d(
            img,
            client=client,
            model_api_name=model_api_name,
            model_id=model_id,
            tag=tag,
            window_title=f"{model_api_name}: {model_id}, patient name: {f.patientName}",
            subplot_titles=f"{model_api_name}: {model_id}"
        )
